##!/bin/sh

BASE="1.1"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

echo "START BUILDING MAC ARM64 VERSION $BASE.$BUILD..."

## linux
env GOOS=darwin GOARCH=arm64 go build -o ./_build/mac_arm64/nftcreator ./main.go


