package main

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_commons"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] APPLICATION '%s' ERROR: %s", nftcreator_commons.AppName, r)
			log.Fatalf(message)
		}
	}()

	//-- command flags --//
	//-- build
	cmdBuild := flag.NewFlagSet("build", flag.ExitOnError)
	cmdBuildConfigurationFile := cmdBuild.String("config", "", "Declare a configuration file")
	cmdBuildSource := cmdBuild.String("source", gg.Paths.Absolute("./source"), "Set a particular folder as main source for image layers")
	cmdBuildMax := cmdBuild.Int("max", 3000, "Max number of images")
	cmdBuildPool := cmdBuild.Int("pool", 15, "Thread Pool limit")
	cmdBuildShuffle := cmdBuild.Bool("shuffle", false, "Shuffle (default=false) before build")
	cmdBuildDirOut := cmdBuild.String("output", gg.Paths.Absolute("./output"), "Set a particular folder as output")
	cmdBuildFormatOut := cmdBuild.String("format", "jpg", "Output format: jpg, png")
	cmdBuildMode := cmdBuild.String("m", nftcreator_commons.ModeProduction, "Mode allowed: 'debug' or 'production'")
	//-- debug
	cmdDebug := flag.NewFlagSet("debug", flag.ExitOnError)
	cmdDebugDirWork := cmdDebug.String("source", gg.Paths.Absolute("./source"), "Set a particular folder as main source for image layers")

	// global parameters
	var cmd string
	settings := &nftcreator_commons.NftCreatorSettings{
		Mode:    nftcreator_commons.ModeProduction,
		Max:     3000,
		Pool:    100,
		Shuffle: false,
		Output:  "./output",
		Source:  "./source",
	}
	if len(os.Args) > 1 {
		cmd = os.Args[1]

		log.Println(fmt.Sprintf("%s running command '%s'...", nftcreator_commons.AppName, cmd))

		switch cmd {
		case "build":
			_ = cmdBuild.Parse(os.Args[2:])

			// preset
			settings.Source = *cmdBuildSource
			settings.Output = *cmdBuildDirOut
			settings.Mode = *cmdBuildMode
			settings.Max = *cmdBuildMax
			settings.Pool = *cmdBuildPool
			settings.Shuffle = *cmdBuildShuffle
			settings.Format = *cmdBuildFormatOut

			if len(*cmdBuildConfigurationFile) > 0 {
				// load config file
				filename := gg.Paths.Absolute(*cmdBuildConfigurationFile)
				if ok, _ := gg.Paths.Exists(*cmdBuildConfigurationFile); ok {
					_ = gg.JSON.ReadFromFile(filename, &settings)
				}
			}
		case "debug":
			_ = cmdDebug.Parse(os.Args[2:])
			settings.Source = *cmdDebugDirWork
			settings.Mode = "debug"
		default:
			panic("Command not supported: " + cmd)
		}
	} else {
		panic("Missing command. i.e. 'build'")
	}

	app, err := nftcreator.LaunchApplication(settings)
	if nil == err {

		err = app.Run(cmd)
		if nil != err {
			log.Panicf("Error running command '%s' in %s: %s", cmd, nftcreator_commons.AppName, err.Error())
		} else {
			log.Println("Elapsed: ", app.Elapsed())
		}
	} else {
		log.Panicf("Error launching %s: %s", nftcreator_commons.AppName, err.Error())
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
