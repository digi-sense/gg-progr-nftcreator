package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_commons"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_rarity"
	"fmt"
	"log"
	"testing"
)

func TestCreatorDebug(t *testing.T) {
	log.Println("START...")

	settings := &nftcreator_commons.NftCreatorSettings{
		Mode:     "debug",
		Pool:     100,
		Max:      2,
		Shuffle:  false,
		Format:   "",
		Source:   "./source",
		Output:   "",
		Metadata: nil,
	}

	// initialize
	creator, err := nftcreator.LaunchApplication(settings)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// count max combo
	max := creator.Count()
	log.Println("Number of maximum combinations: ", max)

	err = creator.Run("debug")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

}

func TestCreatorMatrix(t *testing.T) {
	log.Println("START...")

	settings := &nftcreator_commons.NftCreatorSettings{
		Mode:     "debug",
		Pool:     100,
		Max:      3000,
		Shuffle:  false,
		Format:   "",
		Source:   "./source",
		Output:   "",
		Metadata: nil,
	}

	// initialize
	creator, err := nftcreator.LaunchApplication(settings)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	matrix := creator.GetMatrix()
	expected := creator.Count()
	got := len(matrix)
	fmt.Println("expected: ", expected)
	fmt.Println("got: ", got)
	fmt.Println("matrix", matrix)
	if got != expected {
		t.Error(fmt.Sprintf("Expected '%v' Got '%v'", expected, got))
		t.FailNow()
	}
}

func TestCreator(t *testing.T) {
	log.Println("START...")

	settings := &nftcreator_commons.NftCreatorSettings{
		Mode:     "debug",
		Pool:     100,
		Max:      3000,
		Shuffle:  true,
		Format:   "",
		Source:   "./source",
		Output:   "",
		Metadata: nil,
	}

	// initialize
	creator, err := nftcreator.LaunchApplication(settings)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = creator.Run("build")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestBuild(t *testing.T) {
	name := "test"
	files := []string{
		gg.Paths.Absolute("./source/01-back/back-01.png"),
		gg.Paths.Absolute("./source/02-body/body-01.png"),
		gg.Paths.Absolute("./source/03-extra/extra-01.png"),
	}
	outDir := "./output"
	err := nftcreator_commons.Draw(name, files, outDir, "jpg")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestDuplicate(t *testing.T) {
	imageDir := gg.Paths.Absolute("./output")
	cloneDir := gg.Paths.Absolute("./output-clones")
	dup, err := nftcreator_rarity.NewDuplicator(imageDir, cloneDir, 150, 20, nil)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	count := dup.Count()
	fmt.Println("going to generate: ", count, "images!")

	_, _ = dup.Run(false)
}

func TestCreatorFromZip(t *testing.T) {
	log.Println("START...")

	settings := &nftcreator_commons.NftCreatorSettings{
		Mode:     "debug",
		Pool:     100,
		Max:      300,
		Shuffle:  false,
		Format:   "",
		Source:   "./zip",
		Output:   "",
		Metadata: nil,
	}
	// initialize
	creator, err := nftcreator.LaunchApplication(settings)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	err = creator.Run("build")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}
