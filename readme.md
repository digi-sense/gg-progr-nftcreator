# ![](./icon.png) NFT Creator v0.1.5 #


![](./_graphics/output/graphics_2.png)

With NFT creator you can create your NFT merging some transparent layers
(PNG images) into a unique jpg output.

NFT creator calculate all possible combinations of your assets and creates:

- MODELS: Source images to use as a starting point to produce NFT
- NFT: Final images organized by rarity
- Metadata: All metadata generated with random values and special rules

**TIP**: NFT perfect size is 3000x3000.

![](image-01.jpg)

## Binaries ##

[Binaries are here](https://drive.google.com/drive/folders/1qJ272PrLAfQ6PuykZeNZ4y9yTnoznAQv?usp=sharing).

### Quick Start ###

- [Download](https://drive.google.com/drive/folders/1qJ272PrLAfQ6PuykZeNZ4y9yTnoznAQv?usp=sharing) executable program
  for your Operating System
- Copy into a directory on disk
- Ensure that nftcreator program is executable (i.e. `sudo chmod 777 ./nftcreator`)
- Run it passing desired command and parameters (-source is required)

Run NFTCreator is quite easy.

When you run the `nftcreator` executable you must be sure to use absolute paths for source and output parameters or to
be positioned into executable folder.

For example if you copied NFTCreator executable into `/users/me/NFT/`, you should change directory
using `cd /users/me/NFT/`.

Ensure that nftcreator program is executable (on Mac and Linux: `sudo chmod 777 ./nftcreator`)

**Linux or OSX sample command**

```
./nftcreator build -m=debug -source=./source -output=./output
```

**Windows sample command**

```
./nftcreator.exe build -m=debug -source=./source -output=./output
```

## Supported Commands and Parameters ##

NFT Creator support this commands:

- build: Run program to create both model images and NFT images with standard rarity algorithm
- debug: Run program to write down in console only debug info

### BUILD ###

Build command is for NFT production.

```
./nftcreator build -source=./source
```

**PARAMETERS:**

- `source`: (required) a directory containing all layers or a zip file (containing all layers)
- `output`: (optional) output directory. Default is "./output"
- `max`: (optional) number of NFT you want produce. Default is 3000
- `pool`: (optional) thread pool limit. Default is 100
- `format`: (optional) output format. Default is "jpg". Supported: jpg, png
- `format`: (shuffle) shuffle assets inside layers. Default is "false"
- `m`: (optional) execution mode ("debug", "production"). If "debug" mode is enabled, the program write debug info as
  first console output. Default is "production".

### DEBUG ###

Debug command is intended to test if NFT Creator is working properly and to get info about data that the program will
elaborate to create the output.

```
./nftcreator build -source=./source
```

**PARAMETERS:**

- `source`: (required) a directory containing all layers or a zip file (containing all layers)

Sample output:

```
DEBUG INFO ---------
	* source : /Users/angelogeminiani/go/src/bitbucket.org/digi-sense/gg-progr-nftcreator/_test/zip/source
	* thread pool limit : 100
	* number of layers : 3
		- '01-back': 4 files
			. #0 back-01.png (3002x3002)
			. #1 back-02.png (3002x3002)
			. #2 back-03.png (3002x3002)
			. #3 back-04.png (3002x3002)
		- '02-body': 2 files
			. #0 body-01.png (3000x3000)
			. #1 body-02.png (3000x3000)
		- '03-extra': 3 files
			. #0 extra-01.png (3000x3000)
			. #1 extra-02.png (3000x3000)
			. #2 extra-03.png (3000x3000)
	* maximum NFT production : 24
```

# Advanced Usage 

NFTCreator can run in an advanced mode that can create a directory ready for deploy or upload into IPFS.

This is output "deploy" dir structure:

```
output-deploy
  |____ images
      |__ 1.jpg
      |__ 2.jpg
      |__ ...
  |____ meta
      |__ 1.json
      |__ 2.json
      |__ ...
      
```

This directory contains both images and metadata.

Advanced usage can be enabled using the parameter `-config=PATH_TO_CONFIGURATION_FILE`

For example:
```
./nftcreator build -config=./nftcreator.json
```

## The configuration file ##

```json
{
  "mode": "debug",
  "pool": 100,
  "max": 3000,
  "shuffle": false,
  "format": "jpg",
  "source": "./_test/source",
  "output": "./_test/output",
  "metadata-template": {
    "name": "ID #{{id}}",
    "description": "Sample NFT metadata",
    "image": "ipfs://QmbprqF7kUFZJ3jksnLKo3bLhr1oni3G1T9YOzbnEVXHvh/{{id}}.jpg",
    "attributes": {
      "key": "trait_type",
      "value": "value",
      "values": {
        "Name": [ "<var>user|name</var>" ],
        "Power": ["<var>rnd|between|1-10</var>"],
        "Strength": [ "<var>rnd|between|1-10</var>"]
      }
    }
  }
}
```

As you can see the configuration contains all supported "command-line" parameters, plus a special 
parameter named `metadata-template`.

The `metadata-template` attribute is an object describing how metata must be generated from
NFTCreator.

## The metadata ##

```json
{
  "metadata-template": {
    "name": "ID #{{id}}",
    "description": "Sample NFT metadata",
    "image": "ipfs://QmbprqF7kUFZJ3jksnLKo3bLhr1oni3G1T9YOzbnEVXHvh/{{id}}.jpg",
    "attributes": {
      "key": "trait_type",
      "value": "value",
      "values": {
        "Name": [ "<var>user|name</var>" ],
        "Power": ["<var>rnd|between|1-10</var>"],
        "Strength": [ "<var>rnd|between|1-10</var>"]
      }
    }
  }
}
```

The `metadata-template` objects uses a mustache-like syntax.

Here are all values you can use:

 - `id`: ID of the image. This is always a progressive number.
 - `values`: Key of `values` attribute. Each value Key ("Name", "Power", "Strength") can be used to compose a name or a description.

Example usage of `id`:
```
"name": "ID #{{id}}"
```

Example usage of `values`:
```
"description": "{{Name}} is a warrior with power of #{{Power}}."
```

Moreover, NFTCreator have special superpowers.

Sometimes you may need to generate random numbers, date, names or GUIDs.

You can accomplish this tasks using a special syntax, the `var` tag.

```
"values": {
  "Name": [ "<var>user|name</var>" ],
  "Power": ["<var>rnd|between|1-10</var>"],
  "Strength": [ "<var>rnd|between|1-10</var>"]
}
```

In above example we have "Name", "Power" and "Strength" that are not hardcoded, but are variables 
evaluated at runtime.

### VAR tag usage ###

`var` tag is really a powerful feature that can do some magics.
With this feature you will be able to generate many thousand NFT metadata is just a second.

`var` tag syntax is: TAG_NAME | TAG_OPTIONS. For example: `<var>rnd|between|1-10</var>`. Explain this expression: "rnd" is 
the name of the tag, after the name we can find n options. In this case we have 2 options, 
"between" (that indicates a formula to apply) and "1-10" that is a range of values (from 1 to 10).



Here are some supported var tags:

 - `user`: USER tag can generate fake users with attributes like "name", "surname", "fullname", "country_code", "gender", "email". Sample usage: `<var>user|name</var>`, `<var>user|fullname|3</var>`, `<var>user|fullname, country_code|3</var>`
 - `rnd`: RND tag can generate random numbers or strings. Sample usage: `<var>rnd|between|1-10</var>`, `<var>rnd|id</var>`, `<var>rnd|alphanumeric|6</var>`, `<var>rnd|numeric|6</var>`, `<var>rnd|alphanumeric|6|lower</var>`, `<var>rnd|guid|upper</var>`
 - `date`: DATE tag can generate ISO dates. Sample usage: `<var>date|iso|upper</var>`, `<var>date|yyyy-MM-dd HH:mm|upper</var>`, `<var>date|unix</var>`, `<var>date|ruby</var>`, `<var>date|timestamp_s</var>`, `<var>date|timestamp_m</var>`

**Sample generated metadata:**

```json
{
  "name": "ID #1",
  "description": "Sample NFT metadata",
  "image": "ipfs://QmbprqF7kUFZJ3jksnLKo3bLhr1oni3O1T9YRzbnEVXHvx/1.jpg",
  "attributes": [
    {
      "trait_type": "Name",
      "value": "Emmary"
    },
    {
      "trait_type": "Power",
      "value": "6"
    },
    {
      "trait_type": "Strength",
      "value": "7"
    }
  ]
}
```

If you need further info about NFT metadata, please visit:

 - [OpenSea Doc](https://docs.opensea.io/docs/metadata-standards)
 - [Rarible Doc](https://docs.rarible.org/api-reference/)

## NFT Layers from Photoshop tutorial ##

[https://www.youtube.com/watch?v=k72aCI1VyKM](https://www.youtube.com/watch?v=k72aCI1VyKM)



----

# License #

NFT Creator is free to use for personal and non-commercial use.

You cannot sell NFT Creator, nor sell derivatives or other software or services that use NFT Creator to charge money to
users.

NFT Creator is intended free for private use. If you are a company or a consultant and like NFT Creator also for you
business, please consider ask for a commercial license writing to angelo.geminiani(at)ggtechnologies.sm.

## Startup Special License ##

Startups can ask for a special free license. I'm a startup founder and business angel, and I love support entrepreneur
that are working to change the world for better.

So, If you are a Startup and want to use NFT Creator you can ask me for a Special Startup License just writing me your
website URL and a short description of your project.

----

# Projects using NFT Creator #

Here are projects and companies using NFT Creator in production:

- MOONIA: [Moonia.it](https://moonia.it) is an Italian startup that bring NFT to Companies.
- BOTIKA: [Botika.ai](https://botika.ai) is a Startup based in San Marino that help Banks and Companies to work with
  blockchain.

----

# The Author #

I'm Gian Angelo Geminiani, developer and passionate about Open Source and startups.

My personal project is [G&G Technologies](https://ggtechnologies.sm), a startup working on embedded electronics,
robotics and software.

I'm also founder and CTO of [Botika](https://botika.ai) and [Moonia](https://moonia.it).

## My Books ##

If you like NFT Creator, please consider buying my book
[NFT per Programmatori](https://www.lulu.com/en/en/shop/gian-angelo-geminiani/nft-per-programmatori/paperback/product-27e48w.html?page=1&pageSize=4) (
available only in Italian)

![](./cover.png)

- Buy now on [Lulu.com](https://www.lulu.com/en/en/shop/gian-angelo-geminiani/nft-per-programmatori/paperback/product-27e48w.html?page=1&pageSize=4)
- Buy now on [Amazon](https://www.amazon.it/NFT-Programmatori-corso-programmatori-startup/dp/1794706690/)