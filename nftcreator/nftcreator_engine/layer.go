package nftcreator_engine

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_commons"
	"fmt"
	"image"
	_ "image/png"
	"os"
)

type Layer struct {
	root string

	files []string
}

func NewLayer(root string) (instance *Layer, err error) {
	instance = new(Layer)
	instance.root = root
	err = instance.init()
	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Layer) Name() string {
	if nil != instance {
		return gg.Paths.FileName(instance.root, true)
	}
	return ""
}

func (instance *Layer) Shuffle() []string {
	gg.Arrays.Shuffle(instance.files)
	return instance.files
}

func (instance *Layer) CountImages() int {
	if nil != instance {
		return len(instance.files)
	}
	return 0
}

func (instance *Layer) GetImage(index int) (name string, response image.Image, err error) {
	if nil != instance {
		if index >= len(instance.files) {
			err = gg.Errors.Prefix(nftcreator_commons.OutOfBoundError, fmt.Sprintf("Image index %v does not exists: ", index))
			return
		}
		name = instance.files[index]
		file, ioErr := os.Open(name)
		if nil != ioErr {
			err = ioErr
			return
		}
		response, _, err = image.Decode(file)
		if nil != err {
			return
		}
	}
	return
}

func (instance *Layer) GetImageInfo(index int) (response string, err error) {
	name, img, e := instance.GetImage(index)
	if nil != e {
		err = e
		return
	}
	response = fmt.Sprintf("#%v %s (%vx%v)", index, gg.Paths.FileName(name, true), img.Bounds().Dx(), img.Bounds().Dy())
	return
}

func (instance *Layer) ForImageInfo(callback func(file, info string, err error)) {
	if nil != instance && nil != callback {
		for i, file := range instance.files {
			info, err := instance.GetImageInfo(i)
			callback(file, info, err)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Layer) init() error {
	files, err := gg.Paths.ListFiles(instance.root, "*.png")
	if nil != err {
		return err
	}
	instance.files = files

	return nil
}
