package nftcreator_engine

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_commons"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_rarity"
	_ "image/png"
	"strings"
)

type Engine struct {
	outputType    string // jpg, png
	outputDir     string
	max           int
	metadata      *nftcreator_commons.NftCreatorSettingsMetaData
	pool          *gg_utils.ConcurrentPool
	layers        []*Layer
	matrixRows    int
	matrixMaxCols int
	matrixMinCols int
}

func NewEngine(outputType, outputDir string, layers []*Layer, poolLimit int, max int, metadata *nftcreator_commons.NftCreatorSettingsMetaData) *Engine {
	instance := new(Engine)
	instance.outputType = outputType
	instance.outputDir = outputDir
	instance.layers = layers
	instance.max = max
	instance.metadata = metadata

	instance.pool = gg.Async.NewConcurrentPool(poolLimit)

	instance.matrixRows = len(instance.layers)
	for _, layer := range instance.layers {
		count := layer.CountImages()
		if count > instance.matrixMaxCols {
			instance.matrixMaxCols = layer.CountImages()
		}
		if instance.matrixMinCols == 0 {
			instance.matrixMinCols = count
		} else if count < instance.matrixMinCols {
			instance.matrixMinCols = count
		}
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Engine) Wait() error {
	if nil != instance && nil != instance.pool {
		err := instance.pool.Wait()
		if nil != err.ErrorOrNil() {
			return err.ErrorOrNil()
		}
	}
	return nil
}

func (instance *Engine) Limit() int {
	if nil != instance && nil != instance.pool {
		return instance.pool.Limit()
	}
	return 0
}

func (instance *Engine) Shuffle() {
	if nil != instance {
		for _, layer := range instance.layers {
			_ = layer.Shuffle()
		}
	}
}

func (instance *Engine) MaxCombinations() int {
	if nil != instance {
		response := 1
		for _, layer := range instance.layers {
			response *= layer.CountImages()
		}
		return response
	}
	return 0
}

// GetMatrix calculate matrix and return unique combinations
func (instance *Engine) GetMatrix() []string {
	return instance.getNameMatrix(instance.max)
}

func (instance *Engine) Build() (int, error) {
	outputType := instance.outputType
	outputDir := instance.outputDir
	// get a matrix of all combinations using a triangular matrix
	combinations := instance.GetMatrix()
	if len(combinations) > 0 {
		for _, set := range combinations {
			// transform matrix into file names
			files := instance.getFileNames(set)
			if len(files) > 0 {

				//-- multithreading --//
				instance.pool.RunArgs(func(args ...interface{}) error {
					s := args[0].(string)
					f := args[1].([]string)
					d := args[2].(string)
					t := args[3].(string)
					return nftcreator_commons.Draw(s, f, d, t)
				}, set, files, outputDir, outputType)
			}
		}
		return len(combinations), nil
	}
	return 0, gg.Errors.Prefix(nftcreator_commons.MissingCombinationsError, "Unable to find any combination, please verify debug info: ")
}

func (instance *Engine) MakeClones(generateMetadata bool) (int, error) {
	dupDir := gg.Paths.ChangeFileNameWithSuffix(instance.outputDir, "-deploy")
	dup, err := nftcreator_rarity.NewDuplicator(instance.outputDir, dupDir, instance.max,
		instance.pool.Limit(), instance.metadata)
	if nil != err {
		return -1, err
	}
	return dup.SetRemoveOriginal(false).Run(generateMetadata)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Engine) getNameMatrix(max int) []string {
	response := make([]string, 0)

	matrix := NewMatrix(instance.layers)
	matrix.Scan(func(sequence []int) bool {
		s := gg.Strings.ConcatSep(".", sequence)
		response = gg.Arrays.AppendUnique(response, s).([]string)
		// response = append(response, s)
		if len(response) >= max {
			return false
		}
		return true
	})
	return response
}

func (instance *Engine) getFileNames(set string) (response []string) {
	tokens := strings.Split(set, ".")
	for i, token := range tokens {
		index := gg.Convert.ToInt(token)
		if index > -1 {
			layer := instance.layers[i]
			if nil != layer && len(layer.files) > 0 {
				file := layer.files[index]
				response = append(response, file)
			}
		}
	}
	return
}
