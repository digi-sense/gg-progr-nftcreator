package nftcreator_engine

import (
	"fmt"
	"strings"
)

type Matrix struct {
	layers []*Layer
	cursor map[int]int
	limit  map[int]int
	// history map[int][]int

	rowCount int
	colCount int

	row int
}

func NewMatrix(layers []*Layer) *Matrix {
	instance := new(Matrix)
	instance.layers = layers
	instance.rowCount = len(layers)
	instance.cursor = make(map[int]int)
	instance.limit = make(map[int]int)
	// instance.history = make(map[int][]int)

	instance.Reset()

	return instance
}

func (instance *Matrix) GoString() string {
	return instance.String()
}

func (instance *Matrix) String() string {
	var buf strings.Builder
	buf.WriteString(fmt.Sprintf("Layers: %v\n", instance.rowCount))
	buf.WriteString(fmt.Sprintf("ColMap: %v\n", instance.cursor))
	return buf.String()
}

func (instance *Matrix) RowCount() int {
	return len(instance.layers)
}

func (instance *Matrix) Reset() {
	instance.row = 0
	for i, row := range instance.layers {
		instance.cursor[i] = 0
		// instance.history[i] = make([]int, 0)
		colCount := row.CountImages()
		instance.limit[i] = colCount - 1
		if instance.colCount < colCount {
			instance.colCount = colCount
		}
	}
}

func (instance *Matrix) GetSequence() []int {
	var comb []int
	rows := instance.rowCount
	for i := 0; i < rows; i++ {
		comb = append(comb, instance.cursor[i])
	}
	return comb
}

func (instance *Matrix) Next() bool {
	// move cursor
	if !instance.cursorNext() {
		if !instance.nextRow() {
			// terminated
			return false
		}
	}
	return true // continue
}

func (instance *Matrix) Scan(callback func(sequence []int) bool) (response [][]int) {
	if nil != callback && instance.colCount > 0 && instance.rowCount > 0 {
		// first sequence
		seq := instance.GetSequence()
		response = append(response, seq)
		if callback(seq) {
			for {
				canContinue := instance.Next()
				if !canContinue {
					break
				}
				seq = instance.GetSequence()
				response = append(response, seq)
				if !callback(seq) {
					break
				}
			}
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Matrix) cursorEnd(row int) bool {
	return instance.cursor[row] == instance.limit[row]
}

func (instance *Matrix) cursorNext() bool {
	for i := 0; i <= instance.row; i++ {
		if !instance.cursorEnd(i) {
			instance.cursor[i] = instance.cursor[i] + 1
			//instance.historyAdd(i, instance.cursor[i])
			return true
		} else {
			instance.cursor[i] = 0
			//instance.historyAdd(i, instance.cursor[i])
		}
	}
	return false // cannot move next
}

func (instance *Matrix) nextRow() bool {
	if instance.row+1 == instance.rowCount {
		//miss := instance.historyMiss(instance.row)
		//if miss > -1 {
		//	instance.cursor[instance.row] = miss
		//} else {
		return false // no more rows
		//}
	} else {
		//instance.cursor[0] = 0 // reset first
		if instance.row == 0 {
			instance.row++
			instance.cursor[instance.row] = 0
		} else {
			//miss := instance.historyMiss(instance.row)
			//if miss > -1 {
			//	instance.cursor[instance.row] = miss
			//} else {
			// change row
			instance.row++
			instance.cursor[instance.row] = 0
			// reset history before current row
			//instance.historyResetBefore(instance.row)
			//}
		}
	}

	//instance.historyAdd(instance.row, instance.cursor[instance.row])
	return true
}

/**
func (instance *Matrix) historyAdd(row, value int) {
	history := instance.history[row]
	instance.history[row] = gg.Arrays.AppendUnique(history, value).([]int)
}

func (instance *Matrix) historyExists(row, value int) bool {
	history := instance.history[row]
	return gg.Arrays.IndexOf(value, history) > -1
}

func (instance *Matrix) historyMiss(row int) int {

		limit := instance.limit[row]
		for i := 0; i <= limit; i++ {
			if !instance.historyExists(row, i) {
				return i
			}
		}
	return -1
}

func (instance *Matrix) historyResetBefore(row int) {
	for i := 0; i < row; i++ {
		instance.history[i] = make([]int, 0)
	}
}
**/
