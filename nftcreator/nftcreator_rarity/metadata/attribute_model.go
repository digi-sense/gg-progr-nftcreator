package metadata

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_fnvars"
)

type AttributeModel struct {
	Key    string              `json:"key"`
	Value  string              `json:"value"`
	Values map[string][]string `json:"values"`

	index int
}

// GetAttributesFor return array of attributes
func (instance *AttributeModel) GetAttributesFor(index int, fnVars *gg_fnvars.FnVarsEngine, attrs map[string]interface{}) []map[string]interface{} {
	response := make([]map[string]interface{}, 0)
	fieldKey := instance.Key
	fieldValue := instance.Value
	length := 0
	for k, vv := range instance.Values {
		if len(vv) > 0 && len(k) > 0 {
			if length == 0 {
				length = len(vv)
			}
			value := gg.Arrays.GetAt(vv, index, vv[instance.index])
			item := map[string]interface{}{}
			item[fieldKey] = k
			item[fieldValue] = value
			if nil != fnVars {
				fnVal, err := fnVars.Solve(value)
				if nil == err {
					item[fieldValue] = fnVal
				}
			}
			response = append(response, item)
		}
	}

	// add optional attributes
	if len(attrs) > 0 {
		for k, v := range attrs {
			item := map[string]interface{}{}
			item[fieldKey] = k
			item[fieldValue] = v
			response = append(response, item)
		}
	}

	// inc index as internal counter
	instance.index++
	if instance.index >= length {
		instance.index = 0
	}
	return response
}
