package nftcreator_rarity

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_fnvars"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_commons"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_rarity/metadata"
	"fmt"
	"github.com/cbroglie/mustache"
	"math"
	"sync/atomic"
)

var rarityDistribution = []int{1, 5, 20, 74} // ultra-rare, super-rare, rare, common

type DuplicatorItem struct {
	Count       int      `json:"count"` // to create
	Multiplier  int      `json:"multiplier"`
	Images      []string `json:"images"`
	ImagesCount int      `json:"images_count"`
}

func (instance *DuplicatorItem) String() string {
	return gg.JSON.Stringify(instance)
}

type Duplicator struct {
	rootSource       string
	rootTargetImages string
	rootTargetMeta   string
	filenamePattern  string
	max              int // max number of items to generate
	metadata         *nftcreator_commons.NftCreatorSettingsMetaData

	pool   *gg_utils.ConcurrentPool
	fnVars *gg_fnvars.FnVarsEngine

	removeOriginal bool
	fileImages     []string
	fileAttrs      []string
	count          int
}

func NewDuplicator(sourceRoot, targetRoot string, max, poolLimit int, metadata *nftcreator_commons.NftCreatorSettingsMetaData) (instance *Duplicator, err error) {
	instance = new(Duplicator)
	instance.rootSource = sourceRoot
	instance.rootTargetImages = gg.Paths.Concat(targetRoot, "images")
	instance.rootTargetMeta = gg.Paths.Concat(targetRoot, "meta")
	instance.max = max
	instance.count = max
	instance.metadata = metadata

	instance.pool = gg.Async.NewConcurrentPool(poolLimit)
	instance.fnVars = gg.FnVars.NewEngine()
	t := interface{}(instance.fnVars.GetByName(gg_fnvars.ToolUser))
	if tool, ok := t.(*gg_fnvars.FnVarToolUser); ok {
		// set temp for user data
		tool.SetDataRoot(gg.Paths.GetTempRoot())
	}
	instance.removeOriginal = false

	if len(instance.rootTargetImages) == 0 {
		instance.rootTargetImages = instance.rootSource
	}
	instance.rootTargetImages = gg.Paths.Absolute(instance.rootTargetImages)
	instance.rootTargetMeta = gg.Paths.Absolute(instance.rootTargetMeta)
	instance.rootSource = gg.Paths.Absolute(instance.rootSource)
	_ = gg.Paths.Mkdir(instance.rootTargetImages + gg_utils.OS_PATH_SEPARATOR)
	_ = gg.Paths.Mkdir(instance.rootTargetMeta + gg_utils.OS_PATH_SEPARATOR)

	if len(instance.filenamePattern) == 0 {
		instance.filenamePattern = "counter"
	}

	err = instance.init()

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Duplicator) GetRemoveOriginal() bool {
	if nil != instance {
		return instance.removeOriginal
	}
	return false
}

func (instance *Duplicator) SetRemoveOriginal(value bool) *Duplicator {
	if nil != instance {
		instance.removeOriginal = value
	}
	return instance
}

func (instance *Duplicator) Max() int {
	if nil != instance {
		return instance.max
	}
	return 0
}

func (instance *Duplicator) Count() int {
	if nil != instance {
		return instance.count
	}
	return 0
}

func (instance *Duplicator) Shuffle() *Duplicator {
	gg.Arrays.Shuffle(instance.fileImages)
	return instance
}

func (instance *Duplicator) GetGeneratedImages() (response []string, err error) {
	response = make([]string, 0)
	if nil != instance && len(instance.fileImages) > 0 {
		ext := gg.Paths.ExtensionName(instance.fileImages[0])
		response, err = gg.Paths.ListFiles(instance.rootTargetImages, fmt.Sprintf("*.%s", ext))
	}
	return
}

func (instance *Duplicator) GetGeneratedMetadata() (response []string, err error) {
	response = make([]string, 0)
	if nil != instance {
		response, err = gg.Paths.ListFiles(instance.rootTargetMeta, "*.json")
	}
	return
}

func (instance *Duplicator) Run(generateMetadata bool) (int, error) {
	distribution := instance.distribution()
	var counter uint32 = 0
	var lastCount uint32 = 0
	for _, item := range distribution {
		iToDo := item.Multiplier * item.ImagesCount
		instance.pool.RunArgs(multiplyImagesInPool,
			&counter, instance.fileAttrs, item.Images, instance.rootTargetMeta, instance.rootTargetImages,
			instance.removeOriginal, instance.filenamePattern,
			instance.max, item.Multiplier, lastCount, generateMetadata)
		atomic.AddUint32(&lastCount, uint32(iToDo))
	}
	err := instance.pool.Wait().ErrorOrNil()
	instance.count = int(counter)

	// metadata
	if nil == err && generateMetadata {
		err = instance.generateMetadata()
	}

	return instance.count, err
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Duplicator) init() (err error) {
	instance.fileImages, err = nftcreator_commons.ListImages(instance.rootSource)
	if nil == err {
		instance.fileAttrs, err = nftcreator_commons.ListMeta(instance.rootSource)
		if len(instance.fileImages) == 0 {
			err = gg.Errors.Prefix(nftcreator_commons.NotEnoughImagesError,
				fmt.Sprintf("No image found in folder '%s' : ", instance.rootSource))
		} else {
			if (instance.max - len(instance.fileImages)) < 1 {
				err = gg.Errors.Prefix(nftcreator_commons.NotEnoughImagesError,
					fmt.Sprintf("Too many images to produce something new. Found '%v' images on disk and should generate just '%v': ", len(instance.fileImages), instance.max))
			}
		}
	}
	return
}

func (instance *Duplicator) distribution() (response []*DuplicatorItem) {
	size := len(instance.fileImages)
	lastIndex := 0
	count := 0
	// loop on rarity indexes
	for i, ri := range rarityDistribution {
		item := new(DuplicatorItem)
		response = append(response, item)
		val := int(math.Ceil(float64(size*ri) / float64(100)))
		nr := int(math.Ceil(float64(instance.max*ri) / float64(100)))
		startIndex := lastIndex
		lastIndex = startIndex + val
		if i == len(rarityDistribution)-1 {
			item.Images = instance.fileImages[startIndex:]
			item.Count = instance.max - count
		} else {
			item.Images = instance.fileImages[startIndex:lastIndex]
			item.Count = nr
		}
		item.Multiplier = int(math.Ceil(float64(item.Count) / float64(len(item.Images))))
		item.ImagesCount = len(item.Images)
		count += nr
	}
	return
}

func (instance *Duplicator) generateMetadata() error {
	if nil != instance && nil != instance.metadata && len(instance.metadata.Name) > 0 {
		files, err := instance.GetGeneratedImages()
		if nil != err {
			return err
		}

		// get model for attributes
		var attrModel *metadata.AttributeModel
		err = gg.JSON.Read(gg.JSON.Stringify(instance.metadata.Attributes), &attrModel)
		if nil != err {
			attrModel = nil // no attributes
		}

		for i, image := range files {
			name := gg.Paths.FileName(image, false)
			filename := gg.Paths.Concat(instance.rootTargetMeta, name)
			filename = fmt.Sprintf("%s.json", filename)

			existingMapAttrs := make(map[string]interface{})
			if ok, _ := gg.Paths.Exists(filename); ok {
				m, e := gg.JSON.ReadMapFromFile(filename)
				if nil == e && len(m) > 0 {
					existingMapAttrs = m
				}
			}

			// ready to generate metadata
			instance.metadata.Attributes = getAttributes(i, attrModel, instance.fnVars, existingMapAttrs)
			text := gg.JSON.Stringify(instance.metadata)
			model := map[string]interface{}{
				"id":         name,
				"index":      i,
				"meta-name":  gg.Paths.FileName(filename, true),
				"image-name": gg.Paths.FileName(image, true),
			}
			// merge model with attributes
			attrsArray := gg.Convert.ToArray(instance.metadata.Attributes)
			for _, item := range attrsArray {
				m := gg.Convert.ToMap(item)
				if nil != m {
					k := gg.Reflect.GetString(m, attrModel.Key)
					v := gg.Reflect.GetString(m, attrModel.Value)
					if len(k) > 0 && len(v) > 0 {
						if _, ok := model[k]; !ok {
							model[k] = v
						}
					}
				}
			}

			text, err = mustache.Render(text, model)
			if nil != err {
				return err
			}

			// save meta
			_, err = gg.IO.WriteTextToFile(text, filename)
			if nil != err {
				return err
			}
		}

	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func multiplyImagesInPool(args ...interface{}) error {
	counter := args[0].(*uint32)
	attributes := args[1].([]string)
	images := args[2].([]string)
	targetRootMetas := args[3].(string)
	targetRootImages := args[4].(string)
	removeOriginal := args[5].(bool)
	filenamePattern := args[6].(string)
	globalMax := args[7].(int) // total max number of items
	numberOfCopyPerImage := args[8].(int)
	latest := args[9].(uint32)
	generateMeta := args[10].(bool)

	for _, fileImage := range images {
		if int(latest) >= globalMax {
			break // limit reached
		}
		if (numberOfCopyPerImage + int(latest)) > globalMax {
			numberOfCopyPerImage = globalMax - int(latest)
			if numberOfCopyPerImage < 1 {
				break // trunc to not exceed the global limit
			}
		}
		fileMeta := ""
		if generateMeta {
			fileMeta = lookupFileMeta(fileImage, attributes)
		}
		done, err := nftcreator_commons.Duplicate(fileImage, fileMeta, targetRootImages, targetRootMetas, numberOfCopyPerImage,
			removeOriginal, filenamePattern, int(latest))
		if nil != err {
			return err
		}
		atomic.AddUint32(counter, uint32(done)) // inc counter thread safe
		latest = latest + uint32(done)
	}
	return nil
}

func lookupFileMeta(imageFile string, filesMeta []string) string {
	nameImage := gg.Paths.FileName(imageFile, false)
	for _, fileMeta := range filesMeta {
		nameMeta := gg.Paths.FileName(fileMeta, false)
		if nameMeta == nameImage {
			return fileMeta
		}
	}
	return ""
}

func getAttributes(index int, attrModel *metadata.AttributeModel, fnVars *gg_fnvars.FnVarsEngine, attrs map[string]interface{}) (response []map[string]interface{}) {
	response = make([]map[string]interface{}, 0)
	if nil != attrModel {
		response = attrModel.GetAttributesFor(index, fnVars, attrs)
	}
	return
}
