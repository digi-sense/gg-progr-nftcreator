package nftcreator

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_stopwatch"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_commons"
	"bitbucket.org/digi-sense/gg-progr-nftcreator/nftcreator/nftcreator_engine"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
)

type NftCreator struct {
	settings  *nftcreator_commons.NftCreatorSettings
	mode      string
	root      string
	source    string
	dirLayers string
	dirOutput string

	watch  *gg_stopwatch.StopWatch
	layers []*nftcreator_engine.Layer
	engine *nftcreator_engine.Engine
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *NftCreator) Run(cmd string) (err error) {
	if nil != instance && nil != instance.engine {
		switch cmd {
		case "build":
			instance.watch.Start()
			err = instance.runBuild()
			if nil == err {
				err = instance.engine.Wait()
				if nil == err {
					_, err = instance.engine.MakeClones(true)
				}
			}
			instance.watch.Stop()
		case "debug":
			instance.watch.Start()
			err = instance.runDebug()
			instance.watch.Stop()
		default:
			err = gg.Errors.Prefix(nftcreator_commons.UnsupportedCommandErrorError, fmt.Sprintf("Command not supported '%s': ", cmd))
		}
	}
	return
}

func (instance *NftCreator) Count() int {
	if nil != instance && nil != instance.engine {
		return instance.engine.MaxCombinations()
	}
	return 0
}

func (instance *NftCreator) Elapsed() string {
	if nil != instance && nil != instance.watch {
		return fmt.Sprintf("%v", instance.watch.Elapsed)
	}
	return ""
}

func (instance *NftCreator) GetDebugInfo() (string, error) {
	var buffer strings.Builder
	buffer.WriteString("DEBUG INFO ---------\n")
	buffer.WriteString(fmt.Sprintf("\t* source : %s\n", instance.dirLayers))
	buffer.WriteString(fmt.Sprintf("\t* thread pool limit : %v\n", instance.engine.Limit()))
	buffer.WriteString(fmt.Sprintf("\t* number of layers : %v\n", len(instance.layers)))
	for _, layer := range instance.layers {
		buffer.WriteString(fmt.Sprintf("\t\t- '%s': %v files\n", layer.Name(), layer.CountImages()))
		layer.ForImageInfo(func(file, info string, err error) {
			if nil != err {
				buffer.WriteString(fmt.Sprintf("\t\t\t. %s: '%s'\n", err, gg.Paths.FileName(file, true)))
			} else {
				buffer.WriteString(fmt.Sprintf("\t\t\t. %s\n", info))
			}
		})
	}
	buffer.WriteString(fmt.Sprintf("\t* maximum NFT production : %v\n", instance.Count()))
	return buffer.String(), nil
}

func (instance *NftCreator) GetMatrix() []string {
	if nil != instance && nil != instance.engine {
		return instance.engine.GetMatrix()
	}
	return make([]string, 0)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
func (instance *NftCreator) initPaths() error {
	// paths
	isFile := false
	isArchive := false
	if isFile, _ = gg.Paths.IsFile(instance.source); isFile {
		gg.Paths.GetWorkspace(nftcreator_commons.WpDirWork).SetPath(filepath.Dir(instance.source))
	} else {
		gg.Paths.GetWorkspace(nftcreator_commons.WpDirWork).SetPath(instance.source)
	}
	instance.dirLayers = gg.Paths.GetWorkspace(nftcreator_commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirLayers)
	if len(instance.dirOutput) == 0 {
		instance.dirOutput = gg.Paths.Concat(instance.root, "output")
	} else {
		instance.dirOutput = gg.Paths.Absolute(instance.dirOutput)
	}

	// check for zip file
	if isFile {
		if strings.ToLower(gg.Paths.ExtensionName(instance.source)) == "zip" {
			_, err := gg.Zip.Unzip(instance.source, instance.dirLayers)
			if nil != err {
				return err
			}
			isArchive = true
		}
	} else {
		files, err := gg.Paths.ListFiles(instance.source, "*.zip")
		if nil != err {
			return err
		}
		if len(files) > 0 {
			if len(files) > 1 {
				return gg.Errors.Prefix(nftcreator_commons.RuntimeError, "Too many zip files into source folder: ")
			}
			_, err = gg.Zip.Unzip(files[0], instance.dirLayers)
			if nil != err {
				return err
			}
			isArchive = true
		}
	}

	if isArchive {
		// need to check inside dirLAyes if I found layers or a sub-folder with layers
		dirCount := 0
		dirName := ""
		info, _ := ioutil.ReadDir(instance.dirLayers)
		for _, f := range info {
			if f.IsDir() {
				dirCount++
				dirName = f.Name()
			}
		}
		if dirCount == 1 {
			instance.dirLayers = gg.Paths.Concat(instance.dirLayers, dirName)
		}
	}

	// creates if it does not exist
	_ = gg.Paths.Mkdir(instance.dirLayers + gg_utils.OS_PATH_SEPARATOR)
	_ = gg.Paths.Mkdir(instance.dirOutput + gg_utils.OS_PATH_SEPARATOR)

	return nil
}

func (instance *NftCreator) initLayers() error {
	if len(instance.layers) == 0 {
		// load dirs
		dirs, err := gg.Paths.ListDir(instance.dirLayers) // expected to find layers
		if nil != err {
			return err
		}

		// load layers
		instance.layers, err = loadLayers(dirs)
		if nil != err {
			return err
		}
	}
	return nil
}

func (instance *NftCreator) runDebug() error {
	text, err := instance.GetDebugInfo()

	// write to console
	fmt.Println(text)

	return err
}

func (instance *NftCreator) runBuild() error {
	if instance.mode == nftcreator_commons.ModeDebug {
		err := instance.runDebug()
		if nil != err {
			return err
		}
	}

	// ready to run
	count, err := instance.engine.Build()
	if instance.mode == nftcreator_commons.ModeDebug {
		fmt.Println(fmt.Sprintf("Number of NFT generated: %v", count))
	}
	return err
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func LaunchApplication(settings *nftcreator_commons.NftCreatorSettings) (instance *NftCreator, err error) {
	instance = new(NftCreator)
	instance.settings = settings
	instance.watch = gg.StopWatch.New()
	instance.mode = settings.Mode
	instance.source = gg.Paths.Absolute(settings.Source)
	instance.dirOutput = gg.Paths.Absolute(settings.Output)
	if len(settings.Format) == 0 {
		settings.Format = "jpg"
	}
	if settings.Pool == 0 {
		settings.Pool = 100
	}

	err = instance.initPaths()
	if nil == err {
		// initialize source dir and load layers
		err = instance.initLayers()
		if nil == err {
			instance.engine = nftcreator_engine.NewEngine(settings.Format, instance.dirOutput,
				instance.layers, settings.Pool, settings.Max, settings.Metadata)
			// shuffle layers
			if settings.Shuffle {
				instance.engine.Shuffle()
			}
		}
	}

	return
}

func loadLayers(dirs []string) (response []*nftcreator_engine.Layer, err error) {
	for _, dir := range dirs {
		layer, e := nftcreator_engine.NewLayer(dir)
		if nil != e {
			err = e
			return
		}
		response = append(response, layer)
	}
	return
}
