package nftcreator_commons

import "bitbucket.org/digi-sense/gg-core"

type NftCreatorSettings struct {
	Mode     string                      `json:"mode"`
	Pool     int                         `json:"pool"`
	Max      int                         `json:"max"`
	Shuffle  bool                        `json:"shuffle"`
	Format   string                      `json:"format"`
	Source   string                      `json:"source"`
	Output   string                      `json:"output"`
	Metadata *NftCreatorSettingsMetaData `json:"metadata-template"`
}

func (instance *NftCreatorSettings) String() string {
	return gg.JSON.Stringify(instance)
}

type NftCreatorSettingsMetaData struct {
	Name        string      `json:"name"`
	Description string      `json:"description"`
	Image       string      `json:"image"`
	Attributes  interface{} `json:"attributes"`
}

func (instance *NftCreatorSettingsMetaData) String() string {
	return gg.JSON.Stringify(instance)
}
