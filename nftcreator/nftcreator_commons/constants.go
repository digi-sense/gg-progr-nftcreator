package nftcreator_commons

import "errors"

const (
	AppName    = "NftCreator"
	AppVersion = "0.1.5"

	ModeProduction = "production"
	ModeDebug      = "debug"
)

// ---------------------------------------------------------------------------------------------------------------------
//	w o r k s p a c e s
// ---------------------------------------------------------------------------------------------------------------------

const (
	WpDirStart = "start"
	WpDirApp   = "app"
	WpDirWork  = "*"
)

// ---------------------------------------------------------------------------------------------------------------------
//	e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	RuntimeError                 = errors.New("error_runtime")
	UnsupportedCommandErrorError = errors.New("error_unsupported_command")
	OutOfBoundError              = errors.New("error_out_of_bound")
	MissingCombinationsError     = errors.New("error_missing_combinations")
	NotEnoughImagesError         = errors.New("error_not_enough_images")
)
