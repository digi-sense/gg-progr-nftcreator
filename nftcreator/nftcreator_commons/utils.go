package nftcreator_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"image"
	"image/draw"
	"image/jpeg"
	"image/png"
	_ "image/png"
	"os"
	"strings"
)

func ListImages(root string) ([]string, error) {
	files, err := gg.Paths.ListFiles(root, "*.*")
	if nil != err {
		return nil, err
	}
	images := make([]string, 0)
	for _, file := range files {
		if IsSupportedImage(file) {
			images = append(images, file)
		}
	}
	return images, nil
}

func ListMeta(root string) ([]string, error) {
	files, err := gg.Paths.ListFiles(root, "*.json")
	if nil != err {
		return nil, err
	}
	response := make([]string, 0)
	for _, file := range files {
		response = append(response, file)
	}
	return response, nil
}

func IsSupportedImage(file string) bool {
	ext := gg.Paths.ExtensionName(file)
	return ext == "png" || ext == "jpg" || ext == "jpeg"
}

func Duplicate(sourceImage, sourceMeta, targetRootImage, targetRootMeta string, max int, removeOriginal bool, filenamePattern string, latest int) (done int, err error) {
	done = 0
	count := 0
	for i := 0; i < max; i++ {
		count++

		var targetImage string
		switch filenamePattern {
		case "counter":
			ext := gg.Paths.ExtensionName(sourceImage)
			targetImage = fmt.Sprintf("%v.%s", count+latest, ext)
		default:
			targetImage = gg.Paths.ChangeFileNameWithSuffix(sourceImage, fmt.Sprintf("_%v", i))
		}

		targetImage = gg.Paths.Concat(targetRootImage, gg.Paths.FileName(targetImage, true))
		_, err = gg.IO.CopyFile(sourceImage, targetImage)
		if nil != err {
			return
		}

		// copy meta
		targetMeta := gg.Paths.Concat(targetRootMeta, gg.Paths.FileName(targetImage, false)+".json")
		_, err = gg.IO.CopyFile(sourceMeta, targetMeta)
		if nil != err {
			return
		}

		done++
	}
	// should remove original file?
	if removeOriginal {
		err = gg.IO.Remove(sourceImage)
	}
	return
}

func Draw(set string, files []string, outputDir, imgType string) error {
	// fmt.Println("building: ", set)
	// get images to merge
	images := make([]image.Image, 0)
	for _, file := range files {
		img, err := load(file)
		if err != nil {
			return err
		}
		images = append(images, img)
	}

	//rectangle for the output
	r := image.Rectangle{Max: images[0].Bounds().Max}
	// new image
	dest := image.NewRGBA(r)
	for _, img := range images {
		draw.Draw(dest, img.Bounds(), img, image.Point{}, draw.Over)
	}

	// save
	outputName := gg.Paths.Concat(outputDir, fmt.Sprintf("%s.%s", set, imgType))
	err := save(dest, outputName)
	if nil == err {
		_, err = CreateAttributes(set, files, outputDir)
	}
	return err
}

func CreateAttributes(set string, files []string, outputDir string) (filename string, err error) {
	filename = gg.Paths.Concat(outputDir, fmt.Sprintf("%s.%s", set, "json"))
	attributes := make(map[string]interface{})
	for _, f := range files {
		value := clearName(gg.Paths.FileName(f, false))
		name := clearName(gg.Paths.FileName(gg.Paths.Dir(f), false))
		attributes[name] = value
	}
	_, err = gg.IO.WriteTextToFile(gg.JSON.Stringify(attributes), filename)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func clearName(s string) string {
	tokens := gg.Strings.Split(s, "-_|")
	if len(tokens) > 1 {
		return strings.Join(tokens[1:], "-")
	}
	return s
}

func load(file string) (image.Image, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	img, _, err := image.Decode(f)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func save(img image.Image, file string) error {
	out, err := os.Create(file)
	if err != nil {
		return err
	}

	ext := gg.Paths.ExtensionName(file)
	switch ext {
	case "jpg", "jpeg":
		var opt jpeg.Options
		opt.Quality = 80
		err = jpeg.Encode(out, img, &opt)
		if err != nil {
			return err
		}
	case "png":
		err = png.Encode(out, img)
		if err != nil {
			return err
		}
	default:
		var opt jpeg.Options
		opt.Quality = 80
		err = jpeg.Encode(out, img, &opt)
		if err != nil {
			return err
		}
	}
	return nil
}
