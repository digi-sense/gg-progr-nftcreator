module bitbucket.org/digi-sense/gg-progr-nftcreator

go 1.16

require (
	bitbucket.org/digi-sense/gg-core v0.1.54
	github.com/cbroglie/mustache v1.3.1
	golang.org/x/sys v0.0.0-20220503163025-988cb79eb6c6 // indirect
)
